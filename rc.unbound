#!/bin/sh
# SPDX-License-Identifier: 0BSD
# SPDX-FileCopyrightText: 2023 8B411 <8B411@disroot.org>

readonly usergroup="unbound:unbound"
readonly chroot="/etc/unbound"
readonly pidfile="/var/run/unbound/unbound.pid"
readonly remote="0"

unbound_pid() {
  grep --no-messages "^[0-9]\+$" $pidfile || echo 0
}

unbound_is_running() {
  if [ "$(pgrep -U ${usergroup%%:*} -f ^/usr/sbin/unbound)" = $(unbound_pid) ]; then
    return 0
  else
    return 1
  fi
}

unbound_start() {
  if unbound_is_running; then
    echo "Unbound is already running!"
    return
  fi

  if [ ! -f $chroot/root.key ]; then
    /usr/sbin/unbound-anchor
    chown $usergroup $chroot/root.key
  fi

  if [ $remote = 1 ] && [ ! -f $chroot/unbound_control.key ]; then
    echo "Generating Unbound control key and certificate."
    /usr/sbin/unbound-control-setup -d $chroot > /dev/null 2>&1
    chown $usergroup $chroot/unbound_*key $chroot/unbound_*pem
  fi

  if ! /usr/sbin/unbound-checkconf $chroot/unbound.conf > /dev/null 2>&1; then
    echo "Error in $chroot/unbound.conf, aborted."
    exit 1
  fi

  { mount --bind --no-mtab /dev/random $chroot/dev/random
    mount --bind --no-mtab /dev/log $chroot/dev/log
  } > /dev/null 2>&1

  echo "Starting Unbound:  /usr/sbin/unbound -c $chroot/unbound.conf"
  /usr/sbin/unbound -c $chroot/unbound.conf
}

unbound_stop() {
  echo "Stopping Unbound."
  /bin/kill -s SIGINT $(unbound_pid) > /dev/null 2>&1 && sleep 2

  { umount --no-mtab $chroot/dev/random
    umount --no-mtab $chroot/dev/log
  } > /dev/null 2>&1
}

unbound_reload() {
  if unbound_is_running; then
    echo "Reloading Unbound."
    /bin/kill -s SIGHUP $(unbound_pid) > /dev/null 2>&1
  else
    echo "Unbound is stopped."
  fi
}

unbound_status() {
  if unbound_is_running; then
    echo "Unbound is running (pid $(unbound_pid))."
    exit 0
  else
    echo "Unbound is stopped."
    [ -f $pidfile ] && exit 1 || exit 3
  fi
}

# http://refspecs.linux-foundation.org/LSB_4.1.0/LSB-Core-generic/LSB-Core-generic/iniscrptact.html
case "$1" in
  start)
    unbound_start
  ;;
  stop)
    if ! unbound_is_running; then
      echo "Unbound is not running!"
      exit 7
    else
      unbound_stop
    fi
  ;;
  reload)
    unbound_reload
  ;;
  force-reload)
    unbound_is_running && unbound_reload || unbound_start
  ;;
  restart)
    unbound_is_running && unbound_stop
    unbound_start
  ;;
  try-restart)
    unbound_is_running && unbound_stop && unbound_start
  ;;
  status)
    unbound_status
  ;;
  *)
    echo $"Usage: $0 {start|stop|reload|force-reload|restart|status}"
    exit 2
  ;;
esac

exit 0
